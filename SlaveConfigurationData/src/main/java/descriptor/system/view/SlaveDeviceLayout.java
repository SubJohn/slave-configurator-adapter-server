package descriptor.system.view;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import descriptor.SettingsDescriptor;
import descriptor.config.services.MeasuringCardDescriptor;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonPropertyOrder({ "slaveId","ip","fwVersion","hardwareVersion", "status", "commited","cards" })
public class SlaveDeviceLayout extends SettingsDescriptor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String slaveId;
	private String ip;
	private String fw_version;
	private int status;
	private boolean commited;
	private List<MeasuringCardDescriptor> cards;
	
	
	@Override 
	public boolean equals(Object o) {
		
        if (o == this) {
            return true;
        }
        if ((o == null) || !(o instanceof SlaveDeviceLayout)) {
            return false;
        }
        SlaveDeviceLayout obj = (SlaveDeviceLayout)o;
		boolean equalLists = (this.cards.size() == obj.cards.size()) && (this.cards.containsAll(obj.cards));
		
		if ((this.slaveId.equals(obj.slaveId)) && 
				(this.ip.equals(obj.ip)) &&
				(this.fw_version.equals(obj.fw_version)) && 
				(this.status == obj.status) && 
				(this.commited == obj.commited) &&
				equalLists) {
			return true;
		}
		return false;
	}
	
	public SlaveDeviceLayout() {
		this.cards = new ArrayList<MeasuringCardDescriptor>();
		this.slaveId="";
		this.status = -1;
		this.commited = false;
	}
	
	@JsonIgnore
	public List<MeasuringCardDescriptor> getCardsCopy() {
		return Collections.unmodifiableList(cards);
	}
	
	public List<MeasuringCardDescriptor> getCards() {
		return cards;
	}
	
	public MeasuringCardDescriptor getCard(String cardId) {
		return cards.stream().filter( card -> card.getId().equals(cardId)).findFirst().orElse(null);
	}
	
	public String getSlaveId() {
		return String.copyValueOf(slaveId.toCharArray());
	}

	public String getIp() {
		return String.copyValueOf(ip.toCharArray());
	}
	public String getFw_version() {
		return String.copyValueOf(fw_version.toCharArray());
	}
	
	public int getStatus() {
		return status;
	}
	
	public boolean isCommited() {
		return commited;
	}
	
	public void addToManagedSlaves() {
		this.commited = true;
	}
	
	public SlaveDeviceLayout setMeasuringCards(List<MeasuringCardDescriptor> cards) {
		this.cards = cards;
		return this;
	}
	public SlaveDeviceLayout addMeasuringCard(MeasuringCardDescriptor card) {
		this.cards.add(card);
		return this;
	}
	
	@Override
	public String toString() {
		if(slaveId == null || cards == null) return "";
		return String.format("slaveId: %s | status: %d | commited: %b | cards:\n%s\n",
				slaveId,
				status,
				commited,
				cards.stream()
				.map(MeasuringCardDescriptor::toString )
				.collect(StringBuilder::new,
						(strBuild,ioObjStr) -> { strBuild.append(ioObjStr).append(","); },
						(strBuild1,strBuild2) -> {}).toString());
	}
	
}
