package adapter.slave.config;

import support.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;







public class SlaveConfigurationMain extends HttpServlet {
	


	private static final long serialVersionUID = 1L;
	private static final String SLAVES_CONF_URI = "slave-conf.json";
	private static final String SYSTEM_VIEW_URI = "system-view.json";
	
	 protected void doPost(HttpServletRequest request,
             HttpServletResponse response) throws ServletException, IOException {
		 
		System.out.println("request url:" + request.getRequestURI());
		
		BufferedReader reader = request.getReader();
		String systemSettingsJson = reader
				.lines()
				.collect(StringBuilder::new,StringBuilder::append,StringBuilder::append)
				.toString();
		
		
		//przy mapowaniu calego obiektu Settings, bedzie on mial swoja representacje Json zwracana przez getJsonRepresentation natomiast
		//obiekty skladowe Settings nie beda mialy tej reprezentacji dlatego trzeba to zrobic jawnie.
		var systemSettingsObj = (SystemSettingsManager.Settings)SlaveConfigurationUtils.jsonToSettingsObject(systemSettingsJson, SystemSettingsManager.Settings.class);
		systemSettingsObj.systemServices.setJsonRepresentation( SlaveConfigurationUtils.settingsObjectToJson(systemSettingsObj.systemServices) );
		systemSettingsObj.systemTopology.setJsonRepresentation( SlaveConfigurationUtils.settingsObjectToJson(systemSettingsObj.systemTopology) );
		
		var adapterHostBridge = (AdapterHostBridge)getServletContext().getAttribute("adapterHostBridge");
		
		try {
			
			var changeIndicators = adapterHostBridge.getSystemSettingsManager().analyzeChangeOfSettings(systemSettingsObj);
			
			if(changeIndicators.contains(ConfigStatus.SYSTEM_CONF_CHANGED)) {
				System.out.println("Sending new System Configuration to Host\n");
				adapterHostBridge.sendSystemConfigurationToHost();
			}
			if(changeIndicators.contains(ConfigStatus.SYSTEM_VIEW_CHANGED)) {
				System.out.println("Sending new System View to Host\n");
				adapterHostBridge.sendSystemViewToHost();
			}
			
			var configStatusResponse = SlaveConfigurationUtils.createStatusResponse(ConfigStatus.CONFIG_RECEIVED);
			response.setContentType(WebResourceType.JSON.toString());
			response.getWriter().write(configStatusResponse);
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	
	
	 }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
    	
    	

    	
		System.out.println("request url:" + request.getRequestURL().toString() );

		var context = getServletContext();
		
		var adapterHostBridge = (AdapterHostBridge)context.getAttribute(AdapterHostBridge.CONTEXT_NAME);
		//path usuwa relatywne sciezki do zasobow - zostawia tylko nazwe pliku
		String resourceName = Paths.get(request.getServletPath().substring(1)).getFileName().toString();
		if(resourceName.equals("")) {
			resourceName = "index.html";
		}
		System.out.println("resourceName:" +  resourceName);
		
		
		if(resourceName.equals(SLAVES_CONF_URI)) {
			
	
			String jsonSlavesConfig = adapterHostBridge
					.getSystemSettingsManager()
					.getConfigurationCopy()
					.getJsonRepresentation();
			
			System.out.println("jsonSlavesConfig:" + jsonSlavesConfig);

			response.setContentType(WebResourceType.JSON.toString());
			response.getWriter().write(jsonSlavesConfig != null ? jsonSlavesConfig : "{}");

			return;
		}
		else if(resourceName.equals(SYSTEM_VIEW_URI)) {
			
	
			String jsonSystemView = adapterHostBridge
					.getSystemSettingsManager()
					.getSystemViewCopy()
					.getJsonRepresentation();
			
			System.out.println("jsonSystemView:" + jsonSystemView);
	
			response.setContentType(WebResourceType.JSON.toString());
			response.getWriter().write(jsonSystemView != null ? jsonSystemView : "{}");
			
			return;
		}
		
		// kazdy request o zasob uruchamiany jest w innym watku dlatego trzeba zalozyc
		// mutex (blok synchronized)
		// dla wywolania tej funkcji, poniewaz w przeciwnym razie zachodzi wspolbiezna
		// modyfikacja mapy webResourceCache
		
		synchronized (SlaveConfigurationUtils.webResourceCache) {
			System.out.println("Searching for:" + resourceName);
			try {
				var webResource = SlaveConfigurationUtils.findWebResourceAndCacheIt(
						context.getResource("SlaveConfiguratorUI"), resourceName);
				
				WebResourceType resourceType = webResource.getKey();
				response.setContentType(resourceType.name());
				
				if(resourceType.equals(WebResourceType.IMAGE_JPG) 
						|| resourceType.equals(WebResourceType.IMAGE_PNG)
						|| resourceType.equals(WebResourceType.IMAGE_GIF)) {
					
					System.out.println("sending:" + resourceName);
					response.getOutputStream().write(webResource.getValue());
				}
				else {
					System.out.println("sending:" + resourceName);
					response.getWriter().print( new String(webResource.getValue(), StandardCharsets.UTF_8));
				}
				
			
							
				
			} catch (Exception e) {
				e.printStackTrace();
				//System.out.println("\n=========== resource:" + resourceName + " not finded! ===========\n");
			}
		}
		
    }
}
