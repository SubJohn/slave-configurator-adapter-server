package descriptor.config.services;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

/*
 * 
 * "serviceId": 0,
			"topic": "slave/measure/voltage",
			"tasks": [{
				"taskName": "L1",
				"input": [{
					"cardId": "1234",
					"cardType": 0,
					"ioNum": [1, 3]
				}, {
					"cardId": "5678",
					"cardType": 0,
					"ioNum": [3, 1, 0]
				}]
			}]
 */

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonPropertyOrder({ "serviceId", "topic", "tasks" })
public class ServiceConfigurationDescriptor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int serviceId;
	private String topic;
	private List<ServiceTaskDescriptor> tasks;
	
	@Override 
	public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if ((o == null) || !(o instanceof ServiceConfigurationDescriptor)) {
            return false;
        }
        ServiceConfigurationDescriptor obj = (ServiceConfigurationDescriptor)o;
		boolean equalLists = (this.tasks.size() == obj.tasks.size()) && (this.tasks.containsAll(obj.tasks));
		if ( (topic.equals(obj.topic)) && (serviceId == obj.serviceId) && equalLists) {
			return true;
		}
		return false;
	}
	
	public ServiceConfigurationDescriptor() {
		this.tasks = new ArrayList<ServiceTaskDescriptor>();
		this.serviceId= -1;
		this.topic ="";
	}
	
	public int getServiceId() {
		return serviceId;
	}
	
	public ServiceConfigurationDescriptor setServiceId(int serviceId) {
		this.serviceId = serviceId;
		return this;
	}
	
	public String getTopic() {
		return String.copyValueOf(topic.toCharArray());
	}
	public ServiceConfigurationDescriptor setTopic(String topic) {
		this.topic = topic;
		return this;
	}
	
	public List<ServiceTaskDescriptor> getTasks() {
		return Collections.unmodifiableList(tasks);
	}
	
	public ServiceConfigurationDescriptor setTasks(List<ServiceTaskDescriptor> tasks) {
		this.tasks = tasks;
		return this;
	}
	
	@Override
	public String toString() {
		if(topic == null || tasks == null) return "";
		return String.format("serviceId: %d | topic: %s |  tasks:\n%s\n",
				serviceId,
				topic,
				tasks.stream()
				.map(ServiceTaskDescriptor::toString)
				.collect(StringBuilder::new,
						(strBuild,ioObjStr) -> { strBuild.append(ioObjStr); },
						(strBuild1,strBuild2) -> {}).toString());
	}
}
