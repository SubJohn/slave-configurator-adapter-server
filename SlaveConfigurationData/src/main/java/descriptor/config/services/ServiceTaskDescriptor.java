package descriptor.config.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

/*
 * "tasks": [{
				"taskName": "L1",
				"input": [{
					"cardId": "1234",
					"cardType": 0,
					"ioNum": [1, 3]
				}, {
					"cardId": "5678",
					"cardType": 0,
					"ioNum": [3, 1, 0]
				}]
 */


@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonPropertyOrder({ "taskName", "input" })
public class ServiceTaskDescriptor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String taskName;
	private List<MeasuringCardSourceDataIO> input;
	
	@Override 
	public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if ((o == null) || !(o instanceof ServiceTaskDescriptor)) {
            return false;
        }
        ServiceTaskDescriptor obj = (ServiceTaskDescriptor)o;
		boolean equalLists = (this.input.size() == obj.input.size()) && (this.input.containsAll(obj.input));
		if (taskName.equals(obj.taskName) && equalLists) {
			return true;
		}
		return false;
	}
	public ServiceTaskDescriptor() {
		this.input = new ArrayList<MeasuringCardSourceDataIO>();
		this.taskName = "";
	}
	
	public ServiceTaskDescriptor(String name, List<MeasuringCardSourceDataIO> cardIOs) {
		this.taskName =name;
		this.input = cardIOs;
	}
	@JsonIgnore
	public String getName() {
		return String.copyValueOf(taskName.toCharArray());
	}
	public ServiceTaskDescriptor setName(String name) {
		this.taskName = name;
		return this;
	}
	
	public ServiceTaskDescriptor addSourceDataIO(MeasuringCardSourceDataIO cardIO) {
		this.input.add(cardIO);
		return this;
	}
	@JsonIgnore
	public List<MeasuringCardSourceDataIO> getIOs() {
		return Collections.unmodifiableList(input);
	}
	
	@Override
	public String toString() {
		if(taskName == null) return "";
		return String.format("taskName: %s | input:\n%s\n",
				taskName,
				input.stream()
				.map(MeasuringCardSourceDataIO::toString)
				.collect(StringBuilder::new,
						(strBuild,ioObjStr) -> { strBuild.append(ioObjStr); },
						(strBuild1,strBuild2) -> {}).toString());
	}
}
