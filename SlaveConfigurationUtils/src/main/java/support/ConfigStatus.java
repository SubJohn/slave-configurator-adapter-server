package support;

public enum ConfigStatus {
	 NO_STATUS,
	 CONFIG_RECEIVED,
	 CONFIG_ERROR,
	 SYSTEM_VIEW_CHANGED,
	 SYSTEM_CONF_CHANGED
}
