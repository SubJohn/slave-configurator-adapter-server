//Example of structure output configuration object:
//If new slave will be detected the presence of configuration of this slave in output object
//will be identical with adding slave to database

/*
var systemConfigOutput = {
    config: [{
        slaveId: 111,
        services: [{
            serviceId: 1,
            topic: "bla/bla",
            tasks: [{
                    taskName: "P1",
                    input: [{
                            cardId: 111,
                            cardType: 1,
                            ioLabels: [
                                1
                            ]
                        },
                        {
                            cardId: 112,
                            cardType: 1,
                            ioLabels: [
                                2
                            ]
                        }
                    ]
                },
                {
                    taskName: "P2",
                    input: [{
                        cardId: 112,
                        cardType: 2,
                        ioLabels: [
                            1, 8
                        ]

                    }]
                }
            ]
        }]
    }]
};
*/


var systemConfigOutput = {
    config: []
};


var systemView = {};


var servicesDescriptor = [
    { 
        name: "voltage measure", 
        id: 0 
    },
    { 
        name: "current measure", 
        id: 1 },
    { 
        name: "power measure", 
        id: 2 
    }
];


function getSystemConfigOutputObj() {
    return Object.assign({}, systemConfigOutput);
}

function getSystemViewObj() {
    return Object.assign({}, systemView);
}

function setSystemViewObj(view) {
    systemView = view;
}

function clearSysConfOut() {
    systemConfigOutput = { config: [] };
}

function sysConfOut_AddSlave(slaveId) {
    console.log('sysConfOut_AddSlave:' + slaveId);
    //gdy dodajemy slave niezacomitowanego do powinien nie miec zdefiniowanej
    //zadnej konfiguracji, bo dopiero bedzie ona tworzona, ale jezeli okaze sie
    //ze jednak na Hoscie ma on jakas konfiguracje, to nie tworz nowego obiektu 
    //konfiguracji, tylko pobierz istniejacy
    let slaveObj = systemConfigOutput.config.find( slvConf => slvConf.slaveId === slaveId);
    if(slaveObj == undefined) {
        slaveObj = {};
        slaveObj.slaveId = slaveId;
        slaveObj.services = [];
        systemConfigOutput.config.push(slaveObj);
    }
    //oznaczenie w topologii systemu danego slave'a jako zatwierdzonego
    slaveViewObj = systemView.slavesTopology.find( slvView => slvView.slaveId === slaveId);
    if(slaveViewObj != undefined) {
        slaveViewObj.commited = true;
    }
    console.log(JSON.stringify(systemView));
    console.log(JSON.stringify(systemConfigOutput));
}

/*
slaveDescriptor:  {
                id: <service id>
                topic: <topic>
            }
*/

function sysConfOut_AddService(slaveId, serviceDescriptor) {
    console.log('sysConfOut_AddService');
    console.log('slaveID:' + slaveId);

    let slaveObj = $.grep(systemConfigOutput['config'], obj => {
        return obj.slaveId == slaveId;
    });

    if (slaveObj.length) {
        slaveObj[0]["services"].push({
            serviceId: parseInt(serviceDescriptor.id, 10),
            topic: serviceDescriptor.topic,
            tasks: []
        });
    }


    console.log(systemConfigOutput);
}


function sysConfOut_DeleteService(slaveId, serviceId, topic) {
    console.log('sysConfOut_DeleteService');

    let slaveObj = $.grep(systemConfigOutput['config'], obj => {
        return obj.slaveId == slaveId;
    });

    if (slaveObj) {
        let serviceArray = slaveObj[0]["services"];
        if (serviceArray && serviceArray.length) {
            slaveObj[0]["services"] = serviceArray.filter(serviceObj => {
                return (serviceObj.serviceId != serviceId) || (serviceObj.topic != topic);
            });
        }
    }

    console.log(getSystemConfigOutputObj());

}


function sysConfOut_AddTask(slaveId, serviceId, taskDescriptor) {
    let slaveObj = $.grep(systemConfigOutput['config'], obj => {
        return obj.slaveId == slaveId;
    });
    if (slaveObj && slaveObj.length) {

        let inputs = [];
        let cardsIds = [];
        //pobierz wszystkie id kart jakie zawiera ten slave
        $('#' + slaveId).find('.card').each((i, cardElem) => cardsIds.push($(cardElem).attr('id')));

        $.each(cardsIds, function(i, id) {
            //descriptors zawiera wszsytkie obiekty opisujace wejscia karty o konkretnym id
            let descriptors = taskDescriptor.ios.filter((elem, i, srcArr) => elem.cardId == id);
            if (descriptors.length) {
                let type = descriptors[0].cardType;
                //z obiektow zgromadzonych w descriptors wyciagnij tylko numer IO
                //w ten sposob tablica iosArr zawiera wszystkie uzyte IO dla danej karty
                let iosArr = descriptors.map(d => parseInt(d.ioNum, 10));
                inputs.push({ cardId: id, cardType: parseInt(type), ioNum: iosArr });
            }

        });

        let serviceObj = slaveObj[0]["services"].filter(serviceObj => serviceObj.serviceId == serviceId);
        serviceObj[0]['tasks'].push({
            taskName: taskDescriptor.taskName,
            input: inputs
        });
    }

    console.log("After adding task:");
    console.log(getSystemConfigOutputObj());
    console.log(JSON.stringify(getSystemConfigOutputObj()));
}





function sysConfOut_DeleteTask(slaveId, serviceId, taskName) {
    console.log('sysConfOut_DeleteTask');
    let slaveObj = $.grep(systemConfigOutput['config'], obj => {
        return obj.slaveId == slaveId;
    });
    if (slaveObj.length) {
        if (slaveObj[0]["services"] && slaveObj[0]["services"].length) {
            let serviceObj = slaveObj[0]["services"].filter(serviceObj => serviceObj.serviceId == serviceId);
            let taskArray = serviceObj[0]['tasks'];
            if (taskArray.length) {
                console.log("deleting task: task array before delete:");
                console.log(serviceObj[0]['tasks']);
                serviceObj[0]['tasks'] = taskArray.filter(taskObj => {
                    return taskObj.taskName != taskName;
                });
                console.log("task array after delete:");
                console.log(serviceObj[0]['tasks']);
            }
        }
    }
}




function sysConfIn_Update(sysConfIn) {
    console.log("sysConfIn_Update");

    //przypisanie nowej konfiguracji
    systemConfigOutput = sysConfIn;

    //akualizacja interfejsu uzytkownika
    let slaveConfigArr = systemConfigOutput['config'];

    for (let i = 0; i < slaveConfigArr.length; ++i) {
        console.log("slave id:" + slaveConfigArr[i].slaveId);
        console.log("slave ip:" + slaveConfigArr[i].slaveIp);
        console.log("slave firmware version:" + slaveConfigArr[i].firmwareVersion);
        let slaveId = slaveConfigArr[i].slaveId;
        let $slaveContainer = $('#' + slaveConfigArr[i].slaveId).parent();
        let $slaveInfoObject = $slaveContainer.find('.slave-info');


        let servicesDescriptionArr = slaveConfigArr[i].services;

        for (let j = 0; j < servicesDescriptionArr.length; ++j) {
            
            let servicesNamesMapping = servicesDescriptor.find( service => service.id === servicesDescriptionArr[j].serviceId);
           
            if(servicesNamesMapping != undefined) {

            for (let k = 0; k < servicesDescriptionArr[j].tasks.length; ++k) {
     
                let taskObj = servicesDescriptionArr[j].tasks[k];

                /*
                 ioArr = [ 
                     io-entry {
                        cardId: <string>
                        cardType: <int>
                        ioNum: <int>
                    }, ...
                ]         

                */
                let ioArr = [];
                taskObj.input.forEach(ioObj => {
                    let entry = { cardId: ioObj.cardId, cardType: ioObj.cardType, ioNum: -1 };
                    ioObj.ioNum.forEach(ios => {
                        entry.ioNum = ios;
                        ioArr.push(Object.assign({}, entry));
                    });
                });


        

                $slaveInfoObject.children().last().append( 
                    createServiceSummaryRaport({
                    slaveId:  slaveId,
                    serviceName: servicesNamesMapping != undefined ? servicesNamesMapping.name : null,
                    serviceId: servicesDescriptionArr[j].serviceId,
                    task: taskObj.taskName,
                    topic: servicesDescriptionArr[j].topic,
                    selectedIos:  ioArr
                }));

                $slaveInfoObject.accordion("refresh");
            }   
        }
    }
    }

    console.log("after system info update:");
    console.log(getSystemConfigOutputObj());
}





/*
    zwracana struktura:
    {
        cardId: <string>
        cardType: <int>
        ioNum: <int>
    }

*/

function getIoDescriptrorsFromSelectionList() {
    console.log('getIoDescriptrorsFromSelectionList');
    let $iosObjs = [];
    let $ios = $('#selectedIOS').children();
    $.each($ios, function(index, entry) {
        let $ioNum = $($(entry).children()[0]).text();
        let $cardId = $($(entry).children()[1]).text();
        let $cardType = $('#' + $cardId).find('.card-type .type-value').text();
        $iosObjs.push({ cardId: $cardId, cardType: $cardType, ioNum: $ioNum });
    });
    return $iosObjs;
}



//zwraca referencje do obiektow IO danej karty poprzez wykorzystania
//listy wyboru selectedIOS sluzacej do namierzenia zaznaczonych IO.
//jezeli selectedIO zostanie wczesniej wyczszczone to funkcja nie zwroci
//zazaczonych obiektow IO mimo ze istnieja

function getIoObjsFromSelectionList() {
    console.log('getIoObjsFromSelectionList');
    let $iosElems = [];
    let $iosObjs = getIoDescriptrorsFromSelectionList();
    $.each($iosObjs, function(index, entry) {
        //wybranie obiektu <li> odpowiadajacego danemu wejsciu io
        $iosElems.push($('#' + entry.cardId).find('li.io[value="' + entry.ioNum + '"]'));
    });
    return $iosElems;
}


function isNewSlave(slaveObj) {
    return slaveObj.hasClass('new-slave-marker');
}



function highlightIo(cardId, ioNum) {
    console.log("highlightIo:" + cardId + " io:" + ioNum);
    let ioObj = $('#' + cardId).find('li.io[value="' + ioNum + '"]');
    ioObj.addClass('blink');
}

function unhighlightIo(cardId, ioNum) {
    console.log("unhighlightIo:" + cardId + " io:" + ioNum);
    let ioObj = $('#' + cardId).find('li.io[value="' + ioNum + '"]');
    ioObj.removeClass('blink');

}



function lockIO(cardId, ioNum) {
    console.log("lock:" + cardId + " io:" + ioNum);
    let ioObj = $('#' + cardId).find('li.io[value="' + ioNum + '"]');
    ioObj.addClass('io-lock');
}

function unlockIO(cardId, ioNum) {
    console.log("unlock:" + cardId + " io:" + ioNum);
    let ioObj = $('#' + cardId).find('li.io[value="' + ioNum + '"]');
    ioObj.removeClass('io-lock');
}

function isIoLocked(cardId, ioNum) {
    console.log("isIoLocked:" + cardId + " io:" + ioNum);
    let ioObj = $('#' + cardId).find('li.io[value="' + ioNum + '"]');
    return ioObj.hasClass('io-lock');
}

function isIoUsed(cardId, ioNum) {
    console.log("isIoUsed:" + cardId + " io:" + ioNum);
    let ioObj = $('#' + cardId).find('li.io[value="' + ioNum + '"]');
    return ioObj.hasClass('io-selected');
}

function markIoAsUsed(cardId, ioNum) {
    console.log("markIoAsUsed:" + cardId + " io:" + ioNum);
    let ioObj = $('#' + cardId).find('li.io[value="' + ioNum + '"]');
    ioObj.addClass('io-selected');
}

function unMarkIoAsUsed(cardId, ioNum) {
    console.log("unMarkIoAsUsed:" + cardId + " io:" + ioNum);
    let ioObj = $('#' + cardId).find('li.io[value="' + ioNum + '"]');
    ioObj.removeClass('io-selected');
}


function disableElement(elementObj) {
    console.log("disableElement");
    elementObj.addClass('item-disabled').prop('disabled', true);
}

function enableElement(elementObj) {
    console.log("enableElement");
    elementObj.removeClass('item-disabled').prop('disabled', false);
}


function deleteErrorMsg($parent) {
    console.log('deleteErrorMsg');
    let $errorLabel = $parent.siblings('.error-custom');
    console.log("sibling of:");
    console.log($errorLabel);
    if ($errorLabel.length) {
        $errorLabel.remove();
    }

}

function addErrorMsg($parent, message, addAfterParentElement) {
    let $errorMsgElement = $('<label></label>')
        .addClass('error-custom')
        .text(message);
    if (addAfterParentElement) {
        $parent.after($errorMsgElement);
    } else {
        $parent.before($errorMsgElement);
    }
}


function hashCode(str) {
    var hash = 0;
    if (str.length == 0) return hash;
    for (i = 0; i < str.length; i++) {
        char = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}


//zmienne ustawiane na true gdy pobrane zostana niezbedne dane do prawidlowego wyswietlenia strony
let isSystemViewReady = false;
let isSlaveConfigurationReady = false;

function setSystemViewLoadedFlag() {
    isSystemViewReady = true;
    $('#loading').trigger('finishLoading');
}

function setSlavesConfigurationLoadingFlag() {
    isSlaveConfigurationReady = true;
    $('#loading').trigger('finishLoading');
}

function manageLoadingScreen() {
  /*
    if( isSystemViewReady == true && isSlaveConfigurationReady == true ) {
        $('#loading').fadeOut('slow');
    }
*/
   
    if( isSystemViewReady == true ) {
        $('#loading').fadeOut('slow');
    }
    
}


