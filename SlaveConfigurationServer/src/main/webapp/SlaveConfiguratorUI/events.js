/**************************************** SELECT CARD *****************************************/

function selectCard(ev) {
    ev.stopPropagation();

    $(this).toggleClass('select-class-marker');

    if ($(this).hasClass('select-class-marker')) {
        //ukrycie labelki z id karty
        $(this).find('.slave-id-label').hide();
        $(this).css({
            'position': 'relative',
            'z-index': 2
        });

        $(this).stop().animate({
            left: '5%',
            top: '10%',
        }, {
            easing: 'easeOutBounce',
            duration: 1000
        });
        $(this).find('.card-type').fadeIn(1000);
        $(this).find('.io-list').fadeIn(1000);
    } else {
        $(this).stop().animate({
            left: '0%',
            top: '0%'
        }, {
            done: function() {

                $(this).css({
                    'position': 'static',
                    'z-index': 0
                });
                //pokazanie labelki z id karty
                $(this).find('.slave-id-label').show();
            },
            easing: 'easeInCubic',
            duration: 1000
        });
        $(this).find('.card-type').fadeOut(1000);
        $(this).find('.io-list').fadeOut(1000);
    }

    return true;
}



/**************************************** SELECT SLAVE *****************************************/


function selectSlave(ev) {
    ev.stopPropagation();
    $slaveObj = $('#' + $(this).find(':selected').val()).parent();
    //usun klase pulsing z wszystkich slavow oprocz tych niedodanych do bazy danych
    $('.slave').parent().not('.new-slave-marker').removeClass('pulsing');
    $('#' + $(this).find(":selected").val()).parent().addClass('pulsing');
}


/**************************************** ADD NEW SLAVE *****************************************/

function addNewSlave(ev) {
    console.log('add new slave button')
    ev.stopPropagation();
    let $slaveObj = ev.data.slaveObj;
    //dodaje do globalnego obiektu konfguracji entry z tym slavem
    sysConfOut_AddSlave($slaveObj.find('.slave').attr('id'));
    //dodanie slave do listy wyboru z panelu konfiguracji
    addToSlavesList($slaveObj.children('.slave').attr('id'));

    $slaveObj.removeClass('new-slave-marker').removeClass('pulsing');
    //wyrejestorwanie zdarzenia kliknciecia i usuniecie przeycisku dodawania slave'a
    $slaveObj.find('.slave-info').next().off().remove();
}

/**************************************** SERVICE RAPORT *****************************************/


function taskRaportDeleteEvent(ev) {
    ev.stopPropagation();

    console.log('sys obj BEFORE deleting task:');
    console.log(getSystemConfigOutputObj());
    console.log(systemConfigOutput);

    let $slaveInfoObj = ev.data.slaveInfoObj;
    let $selectedIOsArray = ev.data.selectedIOs;
    let $taskRaportObj = ev.data.taskRaportObj;
    let serviceDescription = ev.data.serviceDescription;

    console.log("<taskRaportDeleteEvent> $selectedIOsArray:");
    console.log($selectedIOsArray);

    $.each($selectedIOsArray, (index, ioObj) => {
        console.log(`disabling io: card: ${ioObj.cardId} | ioNum: ${ioObj.ioNum}`);
        // unlockIO(ioObj);
        unlockIO(ioObj.cardId, ioObj.ioNum);
        //unMarkIoAsUsed(ioObj);
        unMarkIoAsUsed(ioObj.cardId, ioObj.ioNum);
    });



    sysConfOut_DeleteTask(serviceDescription.slaveId, serviceDescription.serviceId, serviceDescription.task);


    console.log('sys obj after deleting task:');
    console.log(getSystemConfigOutputObj());

    $(this).off('click');
    $taskRaportObj.remove();
    $slaveInfoObj.accordion("refresh");

}

function serviceRaportDeleteEvent(ev) {
    ev.stopPropagation();
    let $slaveInfoObj = ev.data.slaveInfoObj;
    let $serviceRaportObj = ev.data.serviceRaportObj;
    let serviceDescription = ev.data.serviceDescription;

    //wywolanie zdarzenia usuwajace poszczegolne taski przed usunieciem serwisu
    $.each($serviceRaportObj.find('.task-summary-raport'), (index, taskRaport) => {
        $(taskRaport).find('.delete-raport-button').trigger('click');
    });

    sysConfOut_DeleteService(serviceDescription.slaveId, serviceDescription.serviceId, serviceDescription.topic);

    //gdy usuwany jest raport calego serwisu to wyczysc liste wyboru slave i service
    clearServiceCustomizationSection();
    $(this).off('click');
    $serviceRaportObj.remove();
    $slaveInfoObj.accordion("refresh");


}

/**************************************** SELECT MENU *****************************************/



function selectSidePannel(ev) {
    $('#side-menu-pannel').toggleClass('open');
    if ($('#side-menu-pannel').hasClass('open')) {
        $('.main').css({
            'transform': 'translate(24%, 0)'
        });
        //gdy otwrzono panel konfiguracyjny to zaprzestan updateow ukladu aby uniknac niespojnosci
        stopPeriodicDownloadSystemView();
        stopPeriodicDownloadSystemConfiguration();
    } else {
        $('.main').css({
            'transform': 'translate(0, 0)'
        });
        //gdy panel konfiguracyjny zostanie zamkniety, to wznow update ukladu strony
        periodicDownloadSystemView(false);
        periodicDownloadSystemConfiguration(false);
    }
}













function clearTaskCustomizationSection() {
    $('#service-task-name').val('');
    $('#selectedIOS').children().remove();
}

function clearServiceCustomizationSection() {
    //slave ma kilka serwisow - wystarczy usunac jeden serwis aby lista byla pusta stad zabezpieczenie
    if ($('#slave-selection-list').val()) {
        $('#' + $('#slave-selection-list').val()).parent().removeClass('pulsing');
        $('#slave-selection-list').prop('selectedIndex', 0);
        $('#topic-input').val('');
    }
}




function addService(ev) {



    ev.preventDefault();
    ev.stopPropagation();
    let selectedIOValidation = true;
    //walidacja czy wybrane zostaly jakiekolwiek IO. Musi byc tutaj
    //bo we wtyczce validation nie da sie podpiac sprawdzanie tego warunku

    //jezeli poprzednio zostala dodana etykieta bledu, to zostaje tu usunieta po czym jest dodana ponownie
    //gdy znowu nie wybrano zadnych IO

    deleteErrorMsg($('#slected-ios-list'));

    if ($('#selectedIOS').children().length == 0) {
        addErrorMsg($('#slected-ios-list'), "Please select at least one card's IO.", false);
     
        selectedIOValidation = false;
    }

    if ($('#service-setup').valid() && selectedIOValidation) {

        let slaveIdVal = $('#slave-selection-list').val();
        let serviceNameVal = $('#service-selection-list').find(':selected').text();
        let serviceIdVal = $('#service-selection-list').find(':selected').val();
        let taskVal = $('#service-task-name').val();
        let topicVal = $('#task-topic-name').val();

        //formowanie danych w json i dodanie ich do globalnego obiektu odpowiedzi dla serwera
        sysConfOut_AddService(
            slaveIdVal, {
                id: serviceIdVal,
                topic: topicVal
            }
        );

        sysConfOut_AddTask(slaveIdVal,
            serviceIdVal, {
                taskName: taskVal,
                ios: getIoDescriptrorsFromSelectionList()
            });

        //aktualizacja Slave-Info 
        //wybranie elementu nalezacego do klasy slave-con (parent()) a nastepnie wybranie elementu nalezacego do klasy slave-info
        $slaveInfoObject = $('#' + $('#slave-selection-list').val()).parent().find('.slave-info');
        //zablokowanie IO, ktore zostaly uzyte do konfiguracji taska serwisu - aby je odblokowac trzeba usunac task serwisu
        //oznaczenie oryginalnych elementow IO karty jako locked
        // $.each(getIoObjsFromSelectionList(), (index, ioObj) => lockIO(ioObj));
        $.each(getIoDescriptrorsFromSelectionList(), (index, obj) => lockIO(obj.cardId, obj.ioNum));


$slaveInfoObject.children().last().append(
    createServiceSummaryRaport({
        slaveId: slaveIdVal,
        serviceName: serviceNameVal,
        serviceId: serviceIdVal,
        task: taskVal,
        topic: topicVal,
        selectedIos:  getIoDescriptrorsFromSelectionList()
    }));

    

        //po dodaniu elementu do widgetu trzeba go odsiewzyc
        $slaveInfoObject.accordion("refresh");

        clearTaskCustomizationSection();

        console.log(getSystemConfigOutputObj());

        return true;
    }

    return false;

}



function commitAllChanges(ev) {
    ev.preventDefault();

    console.log('COMMIT CHANGES');
    let $button = $(this);
    disableElement($button);

    const systemSettings = {
        systemServices : getSystemConfigOutputObj(),
        systemTopology : getSystemViewObj()
    }

    const confStr = JSON.stringify(systemSettings);
    console.log(systemSettings);
    console.log(confStr);

    $.ajax({
        type: 'POST',
        //url: 'SlaveConfigurationServerApp',
        data: confStr,
        processData: false,
        dataType: "json",
        async: true,
        error: function(jqXHR, http_status, exception) {
            console.log("Server not responding!");
            enableElement($button);



        },
        success: function(data, http_status, jqXHR) {

            enableElement($button);

            console.log(data);
            console.log(data.configStatus);

            if (data.configStatus == 'CONFIG_RECEIVED') {
                console.log("delivery success!");
            }

        },
        timeout: 5000
    });

}


/**************************************** SELECT IO *****************************************/

function highlightInputs($cardObj, inputsArray) {
    $cardObj.find('.io').each(function(index) {
        if (inputsArray.indexOf(index) != -1) {
            $(this).addClass('blink');
        } else {
            $(this).removeClass('blink');
        }
    });
}


function setHighlighInput($cardObj, ioNumber, lightUp) {
    $io = $cardObj.find('li.io[value="' + ioNumber + '"]');
    console.log("$io");
    console.log($io);
    if (lightUp) {
        $io.addClass('blink');
    } else {
        $io.removeClass('blink');
    }
}




function selectIO(ev) {
    console.log('select IO');

    ev.stopPropagation();
    let $card = $(this).parents('.card');
    let $cardId = $card.attr('id');
    let ioNumber = $(this).index();

    console.log("$cardId:");
    console.log($cardId);

    //gdy dany IO ma zalozony io-lock oznacza to, ze jest uzywany przez inny task serwisu
    //i nie mozna go wybrac do konfiguracji biezacego taska serwisu. Zdjecie locka
    //nastepuje wraz z usunieciem taska serwisu poprzez wcisniecie przycisku Delete przypisanego
    //do danego taska w Slave-Info

    if (!isIoLocked($cardId, ioNumber) && !isNewSlave($card.parents('.slave_con'))) {

        if (!isIoUsed($cardId, ioNumber)) {

            //unhighlightIo($cardId, ioNumber);
            markIoAsUsed($cardId, ioNumber);
            $('#selectedIOS').append(createIoListEntry({ cardId: $cardId, io: ioNumber }));
            //usuniecie etykiety bledu wystepujacego w przypadku gdy nie wybrano zadnej IO przed przeslaniem formularza
            if ($('#selectedIOS').next().is($('#selectedIOS-error'))) {
                $('#selectedIOS').next().remove();
            }

        } else {
            unMarkIoAsUsed($cardId, ioNumber);
            //highlightIo($cardId, ioNumber);
            $('#selectedIOS').find('div.io-list-entry[id="entity' + ioNumber + '-' + $cardId + '"]').remove();

        }
    } else {
        console.log('io locked!');
    }

}



/**************************************** DOWNLAOD *****************************************/


//to pole jest inicjalizowane slotem na nowy slave gdy przyjdzie ich nieparzysta liczba
//wtedy nastepny slave zostanie umieszczony w tym gotowym juz slocie
var oddFreeSlot = null;

function downlaodSystemView() {

    $.getJSON("system-view.json", function(sys) {

        oddFreeSlot = null;
        //gdy konifugracja jest aktualna server wsyla po prostu pusty obiekt
        if($.isEmptyObject(sys)) return;

        //wyszczyszenie starego ukladu strony i zbudowanie nowego na podstawie nowej konifugracji
        clearSlavesBoard();
        clearSysConfOut();

        console.log(JSON.stringify(sys));

        setSystemViewObj(sys);
        

        let numberOfSlaveSlots = sys.slavesTopology.length;
        let slotsObj = createFields(numberOfSlaveSlots - (oddFreeSlot != null ? -1 : 0));

        let i = 0;
        while ((slotsObj.slots.length - slotsObj.odd) > 0) {

            let slot = oddFreeSlot != null ? oddFreeSlot : slotsObj.slots.shift();
            oddFreeSlot = null;
            let $slaveObj = createSlaveObject(slot, sys.slavesTopology[i]);
            if (sys.slavesTopology[i].commited) {
                sysConfOut_AddSlave(sys.slavesTopology[i].slaveId);
            } else {
                slaveNoPresentDecorator($slaveObj);
            }
            i++;
        }
        //jezeli wyslana zostanie nieparzysta liczba slave'ow to zapisz ostatni slot w zmiennej globalnej
        //Gdy zglosza sie nastepne slavy, pierwszy z nich zajmie wczesniej zaalokowany slot
        if (slotsObj.odd > 0) {
            oddFreeSlot = slotsObj.slots.shift();
        }

        fillServicesList(sys.services);

        setSystemViewLoadedFlag();
    });
}



function downloadSystemConfiguration() {

    $.getJSON("slave-conf.json", function(conf) {
        //gdy konifugracja jest aktualna server wsyla po prostu pusty obiekt
        if($.isEmptyObject(conf)) return;
        console.log("slave conf obj:");
        console.log(conf);
        sysConfIn_Update(conf);
        setSlavesConfigurationLoadingFlag();
    });
}


var downloadSystemViewTimerID = 0;
var downloadSlavesConfigurationTimerID = 0;

function periodicDownloadSystemView(triggerImmediately) {
    if(triggerImmediately) {
        console.log("<periodicDownloadSystemView> trigged immediately");
        downlaodSystemView();
    }
    downloadSystemViewTimerID = setInterval(function() {
        console.log("refresh system view");
        downlaodSystemView();
    
    }, 10000);
}
function periodicDownloadSystemConfiguration(triggerImmediately) {
    if(triggerImmediately) {
        console.log("<periodicDownloadSystemConfiguration> trigged immediately");
        downloadSystemConfiguration();
    }
    downloadSlavesConfigurationTimerID = setInterval(function() {
        console.log("refresh system configuration");
        downloadSystemConfiguration();
    
    }, 10000);
}


function stopPeriodicDownloadSystemView() {
    clearInterval(downloadSystemViewTimerID);
}

function stopPeriodicDownloadSystemConfiguration() {
    clearInterval(downloadSlavesConfigurationTimerID);
}