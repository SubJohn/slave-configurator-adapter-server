module support.SlaveConfigurationUtils {
	exports support;
	requires org.apache.commons.io;
	requires org.apache.commons.lang3;
	requires com.fasterxml.jackson.databind;
	requires transitive data.SlaveConfigurationData;
}