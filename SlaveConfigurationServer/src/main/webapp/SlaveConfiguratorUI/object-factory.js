function creatCardIOs($parentObj, $cardDescriptor,slaveId) {

    let $ioList = $parentObj.find('.io-list');
    $.each($cardDescriptor.ioLabels, function(index, value) {

        let $cardIO = $('<li class="io"><p>' + value + '</p></li>')
            .attr('value', index)
            .on('click', selectIO)
            .on('dblclick',function(ev) {
                $(this).children('p').attr('contenteditable','true').focus();
                highlightIo($cardDescriptor.id,index);
            })
            .focusout(function(ev) {
                unhighlightIo($cardDescriptor.id,index);
                $(this).children('p').attr('contenteditable','false');
                let newIoLabel = $(this).children('p').text();
                let sysView = getSystemViewObj();
                sysView.slavesTopology
                    .find( slvView => slvView.slaveId === slaveId)
                        .cards.find(card => card.id === $cardDescriptor.id)
                            .ioLabels[index] = newIoLabel;
                    
                setSystemViewObj(sysView);
            });
            /*
            $cardIO.children('p').editable({
                  event:'dblclick',
                  closeOnEnter : true,
                  height: 10,
                  callback : function(data) {
                    if( data.content ) {
                        let sysView = getSystemViewObj();
                        sysView.slavesTopology
                            .find( slvView => slvView.slaveId === slaveId)
                                .cards.find(card => card.id === $cardDescriptor.id)
                                    .ioLabels[index] = data.content;
                            
                        setSystemViewObj(sysView);
                    }
                  }
             });
            */

  
        $ioList.append($cardIO);
    });
    $ioList.hide();
}


function createCardObject($parentObj, $cardJsonObj,$slaveJsonObj) {

    let $cardObj = $('<div></div>')
        .attr('id', $cardJsonObj.id)
        .addClass('card')
        .append('<ul class="io-list"></ul>');

    let $slaveIdObj = $('<p></p>').text($cardJsonObj.id).addClass('slave-id-label');

    //labelka z typem karty

    $cardObj.prepend(
        $('<div></div>')
        .addClass('card-type')
        .append($('<p></p>').text('type:'))
        .append($('<p></p>').addClass('type-value').text($cardJsonObj.type))
        .hide()
    );

    $cardObj.append($slaveIdObj);
    //creatCardIOs($cardObj, $cardLayoutJsonObj['card' + $cardJsonObj.type].io);
    creatCardIOs($cardObj,$cardJsonObj,$slaveJsonObj.slaveId);
    //jezeli stan karty jest bledy to nie rejestruj zdarzenia obslugi karty - oznacz karte jako niefunkcjonalna
    //nawet jezeli karta skonfigurowana poprawnie ale slave zwraca blad to nie rejestruj obslugi zdarzen
    if ($cardJsonObj.state == 0 && $slaveJsonObj.status == 0) {
        $cardObj.on('click', selectCard);

    } else {
        $cardObj.addClass('item-disabled');
        if ($cardJsonObj.state != 0) {
            $cardObj.append($('<p></p>')
                .addClass('card-error-code')
                .text('invalid state [code:' + $cardJsonObj.state + ']'));
        }

    }
    //przypsanie typu karty
    $parentObj.append($cardObj);
}


function clearSlaveList() {
    $('#slave-selection-list').empty().append( 
        $('<option></option>')
        .val("")
        .prop("hidden",true)
        .attr("selected","selected")
        .text("select slave")
    );  
    console.log("SLECTION LIST SLAVE");
    console.log($('#slave-selection-list'));
}

function addToSlavesList(id) {
    $('#slave-selection-list').append($('<option></option>').attr('value', id).text(id));
}



function createSlaveObject($parentObj, $slaveJsonObj) {
    let $slaveObj = $('<div></div>').addClass('slave').attr("id", $slaveJsonObj.slaveId);
    $.each($slaveJsonObj.cards, function(index, data) {
        createCardObject($slaveObj, data,$slaveJsonObj);
    });
    let $slaveInfoObj = $('<div></div>')
        .addClass('slave-info')
        .append('<h3>Slave Info</h3>')
        .append('<div id></div>');

    console.log('status:' + $slaveJsonObj.status);

 
    //dodaj slave do listy wyboru z panelu konfiguracji tylko gdy slave dodany jest do konfiguracji systemu
    if($slaveJsonObj.commited == true) {
        addToSlavesList($slaveJsonObj.slaveId);
    }

    $slaveInfoObj.accordion({
        active: false,
        collapsible: true,
        heightStyle: 'auto',
        disabled: $slaveJsonObj.status != 0
    });

    let $slaveContainer = $('<div></div>').addClass('slave_con')
        .append('<p class="slave-id">' + $slaveJsonObj.slaveId + '</p>').append($slaveObj).append($slaveInfoObj);
    $parentObj.append($slaveContainer);

    //status rozny od 0 oznacza ze cos jest nie tak ze slavem
    if ($slaveJsonObj.status != 0) {
        $slaveContainer.addClass('slave-bad-state-marker');
        $slaveInfoObj.after($('<p></p>').addClass('slave-bad-state-error-message').text('bad status: [code:' + $slaveJsonObj.status + ']'));
    }

    $slaveInfoObj.children().last().append( 
        createAdditionalSlaveInfoObject(
            {
                ip :  $slaveJsonObj.ip,
                fwVersion :  $slaveJsonObj.fw_version
            }
        )
    );

    $slaveInfoObj.accordion("refresh");


    console.log("slave container:" + $slaveContainer.html());



    return $slaveContainer;
}


function slaveNoPresentDecorator($slaveObj) {
    $slaveObj.addClass('new-slave-marker').addClass('pulsing');

    let $addSlaveButton = $('<input></input>')
        .attr('type', 'button')
        .addClass('button-primary')
        .addClass('add-new-slave-button')
        .val('Add Slave Device');

    $slaveObj.find('.slave-info').after($addSlaveButton);

    $addSlaveButton.on('click', { slaveObj: $slaveObj }, addNewSlave);

}



function clearSlavesBoard() {
    $('.main').empty();
    clearServicesList();
    clearSlaveList();
}

function createFields(numberOfFileds) {
    let $main = $('.main');
    let rowsNum = Math.floor(numberOfFileds / 2);
    let isOdd = numberOfFileds % 2;

    let fields = [];
    for (let i = 0; i < rowsNum + isOdd; ++i) {
        let $row = $('<div></div>').addClass('row');
        let $column1 = $('<div></div>').addClass('six').addClass('columns');
        let $column2 = $('<div></div>').addClass('six').addClass('columns');
        $row.append($column1).append($column2);
        $main.append($row);
        fields.push($column1);
        fields.push($column2);
    }
    return {
        odd: isOdd,
        slots: fields
    };
}

function clearServicesList() {
    $('#service-selection-list').empty().append(
        $('<option></option>')
        .val("")
        .prop("hidden",true)
        .attr("selected","selected")
        .text("select service")
    );

}

function fillServicesList() {
    $servicesList = $('#service-selection-list');
    for (let i = 0; i < servicesDescriptor.length; ++i) {
        $servicesList.append($('<option></option>')
            .attr('value', servicesDescriptor[i].id)
            .text(servicesDescriptor[i].name));
    }
}


function createIoListEntry(ioListEntryObj) {
    return $('<div></div>')
        .attr('id', 'entity' + ioListEntryObj.io + '-' + ioListEntryObj.cardId)
        .addClass('io-list-entry')
        .append('<p>' + ioListEntryObj.io + '</p>')
        .append('<p>' + ioListEntryObj.cardId + '</p>');
}



/**************************************** SERVICE RAPORT *****************************************/



//tworzy obiekt raporty przypinany do listy Slave-Info w wybranym obiekcie Slave

/*
Struktura obiektu przyjmowanego jako parametr
{
    slaveId:
    serviceName:
    serviceId:
    task:
    topic:
}
*/


function createServiceRaportElement(serviceRaportId, dataObj) {

    $serviceRaportElement = $('<div></div>').addClass('slave-summary-raport').attr('id', serviceRaportId)
        .append( 
            $('<div></div>')
            .append($('<h></h>').text('Service name: '))
            .append($('<p></p>').addClass('summary-raport-service-name-marker').text(dataObj.serviceName))  
        )
        .append(
            $('<div></div>')
            .append($('<h></h>').text('Topic: '))
            .append($('<p></p>').addClass('summary-raport-topic-marker').text( dataObj.topic ))
        );
    
    //przycisk pozwoli usunac dany serwis z listy slave-info i wylaczyc ten serwis w systemie
    let $serviceRaportSelfDelete = $('<div></div>')
        .addClass('delete-raport-button')
        .on('click', {
            serviceDescription: dataObj,
            serviceRaportObj: $serviceRaportElement,
            slaveInfoObj: $('#' + dataObj.slaveId).parent().find('.slave-info'),
        }, serviceRaportDeleteEvent);
    $serviceRaportElement.append($serviceRaportSelfDelete);

    return $serviceRaportElement;
}



/*
    ioObjArr : [
        { cardId: <cardId>, cardType: <type>, ioNum: <io-num> }
    ]

     dataObj {
        slaveId: <slaveId>
        serviceName: <serviceName>
        serviceId: <serviceId>>
        task: <task>
        topic: <topic>
 }
*/

function createServiceTaskRaportElement(taskRaportId, dataObj, ioObjArr) {


    let $ioListElement = $('<ul></ul>');
    console.log("ioObjArr.length:");
    console.log(ioObjArr.length);
    //posortuj io wedlug numeru
    if( ioObjArr.length > 1) {
        ioObjArr.sort((e1, e2) => e1.ioNum > e2.ioNum ? 1 : e1.ioNum == e2.ioNum ? 0 : -1);
    }
    for (let i = 0; i < ioObjArr.length; ++i) {
        $ioListElement.append(createIoListEntry({ cardId: ioObjArr[i].cardId, io: ioObjArr[i].ioNum }));
        markIoAsUsed(ioObjArr[i].cardId,ioObjArr[i].ioNum);
        lockIO(ioObjArr[i].cardId,ioObjArr[i].ioNum);
    }
    console.log("ioObjArr:");
    console.log(ioObjArr);

    console.log("$ioListElement");
    console.log($ioListElement);

    let $taskRaportElement = $('<div></div>')
        .addClass('slave-summary-raport task-summary-raport')
        .attr('id', taskRaportId)
        .append( 
            $('<div></div>')
            .append($('<h></h>').text('Task: '))
            .append($('<p></p>').addClass('summary-raport-task-marker').text(dataObj.task))  
        )
        .append(
            $('<div></div>')
            .append($('<h></h>').text('Data Inputs:'))
            .append($ioListElement)
        );

    let $taskRaportSelfDelete = $('<div></div>')
        .addClass('delete-raport-button')
        //nalezy przekazac tablice selectedIOs poniewaz gdyby uzyc funkcji getIoObjsFromSelectionList() wewnatrz zdarzenia
        //to zwrocilaby ona pusta tablice, bo po zakonczeniu tej funkcji a nastepnie funkcji addServiceTask lista zostaje oprozniona
        .on('click', {
            serviceDescription: dataObj,
            taskRaportObj: $taskRaportElement,
            slaveInfoObj: $('#' + dataObj.slaveId).parent().find('.slave-info'),
            selectedIOs: ioObjArr
        }, taskRaportDeleteEvent);

    $taskRaportElement.append($taskRaportSelfDelete);

    return $taskRaportElement;
}






/*
 dataObj {
        slaveId: <slaveId>
        serviceName: <serviceName>
        serviceId: <serviceId>>
        task: <task>
        topic: <topic>
        selectedIos: <io-entry>
 }

  io-entry {
        cardId: <string>
        cardType: <int>
        ioNum: <int>
    }

*/

function createServiceSummaryRaport(dataObj) {

    let $slectedIOsList = $('#selectedIOS');
    let $selectedIOsObjects = getIoObjsFromSelectionList();
    console.log("selected objects:" + $selectedIOsObjects);
    let cardId = dataObj.selectedIos[0].cardId;

    //utworzenie unikalnego id dla summary. o unikalnosci decyduja indeksy wybranych IO poniewaz w ramach
    //jednej karty sa one unikalne oraz id karty


    let serviceRaportId = dataObj.slaveId + '-' + dataObj.serviceId + hashCode(dataObj.topic);

    let $serviceRaportElement = $('#' + serviceRaportId);

    //taki serwis juz istnieje w tym slave
    if ($serviceRaportElement.length == 0) {
       
        $serviceRaportElement = createServiceRaportElement(serviceRaportId, dataObj);
    }



    let taskRaportId = dataObj.slaveId + '-' + dataObj.serviceId + '-' + dataObj.task + '-' + cardId + '-' + dataObj.selectedIos.map(x => x.ioNum).join();
    console.log('taskRaportId:' + taskRaportId);

    //proba dodania do danego serwisu taska o identycznej etykiecie - wyswietl komunikat bledu
    //containst zwraca element, ktory zawiera tekst podany w selektorze, a nie jest identyczny, stad jeszcze funkcja filtrujaca
    let duplicatedTask = $serviceRaportElement
        .find('p.summary-raport-task-marker:contains(' + dataObj.task + ')')
        .filter(( i,val ) => $(val).text() === dataObj.task);

    //jezeli podobny komunikat juz istnieje to usun go aby nie duplikowac komunikat w przypadku ponowie zle wprowadzonej wartosci
    deleteErrorMsg($('#add-task-button'));

    if (duplicatedTask && duplicatedTask.length != 0) {
        addErrorMsg($('#add-task-button'), 'Such a task already exists for this service!', true);
        dataObj.selectedIos.forEach((value, i, arr) => {
            unMarkIoAsUsed(cardId, value.ioNum);
            unlockIO(cardId,value.ioNum);
        });
    } else {

        let $taskRaportElement = createServiceTaskRaportElement(taskRaportId, dataObj, dataObj.selectedIos);
        
        $serviceRaportElement.append($taskRaportElement);
    }

    console.log($serviceRaportElement);

    return $serviceRaportElement;

}

function createAdditionalSlaveInfoObject(additionalInfoObj) {

    let slaveIpObj = $('<div></div>')
        .addClass('slave-additional-info')
        .append($('<h></h>').text('IP: '))
        .append($('<p></p>').text(additionalInfoObj.ip));

    let firmwareVersionObj = $('<div></div>')
        .addClass('slave-additional-info')
        .append($('<h></h>').text('Firmware: '))
        .append($('<p></p>').text(additionalInfoObj.fwVersion));

    return $('<div></div>')
        .addClass('slave-additional-info-container')
        .append(slaveIpObj)
        .append(firmwareVersionObj);
}
